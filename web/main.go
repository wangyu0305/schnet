package main

import (
	"gopkg.in/mgo.v2"
	"crypto/sha256"
	"encoding/hex"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/gorilla/securecookie"
	"net/http"
	"time"
	"fmt"
	"os"
	"path/filepath"
)

const (
	PORT = ":10443"
	PRIV_KEY = "server.crt"
	PUBLIC_KEY = "server.key"
	MEMCACHE = "127.0.0.1:11211"
)

func getAppDir() string {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		panic(err)
	}
	return dir
}

// Settings
var (
	dir        = getAppDir()
	staticpath = dir + "/static/"
	imgpath    = staticpath + "img/"
	csspath    = staticpath + "css/"
)

var mc = memcache.New(MEMCACHE)

type Configuration struct {
	memcache string
}

type Page struct {
	Title string
	Body  []byte
}

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

func main() {
	http.HandleFunc("/", home)
	http.HandleFunc("/login", login)
	http.HandleFunc("/logout", logout)
	http.HandleFunc("/account", account)
	http.HandleFunc("/background", background)
	http.HandleFunc("/course", course)
	http.HandleFunc("/course/channel", channel)
	http.HandleFunc("/question", question)
	http.HandleFunc("/idea", idea)
//	http.Handle("/static/", http.StripPrefix("/static/", http.FileServer(http.Dir(staticpath))))
//	http.Handle("/img/", http.StripPrefix("/img/", http.FileServer(http.Dir(imgpath))))
//	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir(csspath))))
	err := http.ListenAndServeTLS(PORT, PRIV_KEY, PUBLIC_KEY, nil)
	if err != nil {
		logging(fmt.Sprintf("ERROR: certificate error : date=%s, msg=%s", time.Now(), err))
	}
}

func mgoInit() *mgo.Session {
	mongoSession, err := mgo.Dial(conf.Mongo["HOST"])
	if err != nil {
		panic(err)
	}
	mongoSession.SetSafe(&mgo.Safe{})
	return mongoSession
}

func mgoRelease(s *mgo.Session) {
	s.Close()
}

func checkLogin(r *http.Request) (string, bool) {
	_, c, err := getSession(r)
	if c != nil {
		login, err := mc.Get(c)
		if err == nil && login.Object["login"] {
			return login.Object["id"], true
		} else {
			logging(fmt.Sprintf("INFO: failure login attemp: date=%s msg=%s", time.Now(), err))
			return nil, false
		}
	} else {
		logging(fmt.Sprintf("INFO: session error: date=%s msg=%s", time.Now(), err))
		return nil, false
	}
}

func setSession(userName string, response http.ResponseWriter) {
	value := map[string]string{
		"name": userName,
	}
	if encoded, err := cookieHandler.Encode("session", value); err == nil {
		cookie := &http.Cookie{
			Name:  "session",
			Value: encoded,
			Path:  "/",
		}
		http.SetCookie(response, cookie)
		status := map[string]interface{}{"login": true, "id": encoded}
		mc.Set(&memcache.Item{Key: userName, Object: status, Expiration: time.Hour * 72})
	}
}

func getSession(request *http.Request) (string, string, error) {
	if cookie, err := request.Cookie("session"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
			userName := cookieValue["name"]
			return userName, cookie, nil
		}
	}
	return
}

func clearSession(response http.ResponseWriter, sessionCookie string) {
	cookie := &http.Cookie{
		Name:   "session",
		Value:  "",
		Path:   "/",
		MaxAge: -1,
	}
	http.SetCookie(response, cookie)
	status := map[string]interface{}{"login": false, "id": ""}
	mc.Set(&memcache.Item{Key: sessionCookie, Object: status, Expiration: time.Hour * 72})
}

func encrypt(data string) string {
	hash := sha256.New()
	hash.Write(data)
	md := hash.Sum(nil)
	return hex.EncodeToString(md)
}
