package draw

import (
	"github.com/llgcode/draw2d/draw2dimg"
	"image"
	"image/color"
	"github.com/llgcode/draw2d"
	"golang.org/x/image/math/fixed"
	"math/rand"
	"time"
	"image/jpeg"
	"os"
	"bufio"
)

// stand for color red, orange, yellow, green, cyan, blue, purple
var colorMap = map[uint8]color.RGBA{
	0: color.RGBA{randInt(220, 240), randInt(40, 70), randInt(40, 70), 0xff},
	1: color.RGBA{randInt(230, 255), randInt(95, 135), randInt(4, 50), 0xff},
	2: color.RGBA{randInt(250, 255), randInt(245, 255), randInt(0, 80), 0xff},
	3: color.RGBA{randInt(30, 100), randInt(250, 255), randInt(0, 100), 0xff},
	4: color.RGBA{randInt(0, 100), randInt(250, 255), randInt(200, 220), 0xff},
	5: color.RGBA{randInt(0, 80), randInt(70, 130), randInt(220, 255), 0xff},
	6: color.RGBA{randInt(160, 200), randInt(0, 100), randInt(200, 255), 0xff}}

func drawH(text string, fd draw2d.FontData, size float64, c, stroke, fill color.Color) (buffer image.Image) {
	const stretchFactor = 1.25
	var widthStretch float64
	height := 125.0
	widthMax := 450.0

	switch len(text) {
	case 1:
		widthStretch = widthMax / 2
	case 2:
		widthStretch = widthMax / 2.5
	case 3:
		widthStretch = widthMax / 3
	case 4:
		widthStretch = widthMax / 4
	case 5:
		widthStretch = widthMax / 6
	case 6:
		widthStretch = widthMax / 8
	case 7:
		widthStretch = widthMax / 11
	case 8:
		widthStretch = widthMax / 15
	case 9:
		widthStretch = 5
	case 10:
		widthStretch = 1
	default:
		widthStretch = 0
	}

	fImg1, _ := os.Open("background1.png")
	defer fImg1.Close()
	img1, _, _ := image.Decode(fImg1)
	var b = img1.Bounds()
	var randX = randInt32(b.Min.X, b.Max.X - int(widthMax))
	var randY = randInt32(b.Min.Y, b.Max.Y - int(height))

	buf := image.NewRGBA(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})

	for x0, x := 0, randX; x0 <= int(widthMax); x++ {
		for y0, y := 0, randY; y0 <= int(height); y++ {
			col := img1.At(x, y)
			buf.Set(x0, y0, col)
			y0++
		}
		x0++
	}

	gc := draw2dimg.NewGraphicContext(buf)

	//	x0, y0 := 0.0, 0.0
	//	x1 := x0 + widthMax
	//	y1 := y0 + height
	//	gc.MoveTo(x0, (y0 + y1) / 2)
	//	gc.CubicCurveTo(x0, y0, x0, y0, (x0 + x1) / 2, y0)
	//	gc.CubicCurveTo(x1, y0, x1, y0, x1, (y0 + y1) / 2)
	//	gc.CubicCurveTo(x1, y1, x1, y1, (x1 + x0) / 2, y1)
	//	gc.CubicCurveTo(x0, y1, x0, y1, x0, (y0 + y1) / 2)
	//	gc.SetStrokeColor(stroke)
	//	gc.SetFillColor(fill)
	//	gc.SetLineWidth(10.0)
	//	gc.FillStroke()

	gc.Translate(widthStretch, height / stretchFactor)
	gc.SetFontData(fd)
	gc.SetFontSize(size)
	gc.SetFillColor(c)
	gc.FillString(text)

	buffer = buf.SubImage(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})
	// Save to file
	return buffer
}

func goGraphics() {
	fImg1, _ := os.Open("background1.png")
	defer fImg1.Close()
	img1, _, _ := image.Decode(fImg1)

	fImg2, _ := os.Open("background2.jpg")
	defer fImg2.Close()
	img2, _, _ := image.Decode(fImg2)

	m := image.NewRGBA(image.Rect(0, 0, 400, 300))
	//	draw.Draw(m, m.Bounds(), img1, image.Point{0, 0}, draw.Src)
	//	draw.Draw(m, m.Bounds(), img2, image.Point{-200,-200}, draw.Src)
	//	graphics.Rotate(m, img2, &graphics.RotateOptions{3.5})
	for x := 0; x <= 200; x++ {
		for y := 0; y < 150; y++ {
			col := img1.At(x, y)
			m.Set(x, y, col)
		}
	}
	for x := 200; x <= 400; x++ {
		for y := 150; y < 300; y++ {
			col := img2.At(x, y)
			m.Set(x, y, col)
		}
	}
	toimg, _ := os.Create("new.jpg")
	defer toimg.Close()

	jpeg.Encode(toimg, m, &jpeg.Options{jpeg.DefaultQuality})
}

func LoadFromJpegFile(filePath string) (image.Image, error) {
	// Open file
	f, err := os.OpenFile(filePath, 0, 0)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	b := bufio.NewReader(f)
	img, err := jpeg.Decode(b)
	if err != nil {
		return nil, err
	}
	return img, nil
}

func GetFontHeight(fd draw2d.FontData, size float64) (height float64) {
	font := draw2d.GetFont(fd)
	fupe := font.FUnitsPerEm()
	bounds := font.Bounds(fixed.Int26_6(fupe))
	height = float64(bounds.Max.Y - bounds.Min.Y) * size / float64(fupe)
	return height
}

func RenderString(text string, fd draw2d.FontData, size float64, c, stroke, fill color.Color) (buffer image.Image) {

	const stretchFactor = 1.75
	var widthStretch float64
	height := 150.0
	widthMax := 150.0

	switch len(text) {
	case 1:
		widthStretch = widthMax / 3.5
	case 2:
		widthStretch = widthMax / 7
	case 3:
		widthStretch = 1
	}

	buf := image.NewRGBA(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})

	gc := draw2dimg.NewGraphicContext(buf)

	x0, y0 := 0.0, 0.0
	x1 := x0 + widthMax
	y1 := y0 + height
	gc.MoveTo(x0, (y0 + y1) / 2)
	gc.CubicCurveTo(x0, y0, x0, y0, (x0 + x1) / 2, y0)
	gc.CubicCurveTo(x1, y0, x1, y0, x1, (y0 + y1) / 2)
	gc.CubicCurveTo(x1, y1, x1, y1, (x1 + x0) / 2, y1)
	gc.CubicCurveTo(x0, y1, x0, y1, x0, (y0 + y1) / 2)
	gc.SetStrokeColor(stroke)
	gc.SetFillColor(fill)
	gc.SetLineWidth(10.0)
	gc.FillStroke()

	gc.Translate(widthStretch, height / stretchFactor)
	gc.SetFontData(fd)
	gc.SetFontSize(size)
	gc.SetFillColor(c)
	gc.FillString(text)

	buffer = buf.SubImage(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})
	return buffer
}

func randInt(min, max int) uint8 {
	rand.Seed(time.Now().UTC().UnixNano())
	return uint8(min + rand.Intn(max - min))
}

func randInt32(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return int(min + rand.Intn(max - min))
}