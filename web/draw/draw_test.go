package draw

import (
	"testing"
	"github.com/llgcode/draw2d"
	"image/color"
	"os"
	"image"
	"github.com/llgcode/draw2d/draw2dimg"
)

var fontPath = os.Getenv("GOPATH") + "/src/github.com/llgcode/draw2d/resource/font" //stupid

func TestDraw(t *testing.T) {
	draw2d.SetFontFolder(fontPath)
	var fontdata = draw2d.FontData{
		Name:   "luxi",
		Family: draw2d.FontFamilyMono,
		Style:  draw2d.FontStyleBold | draw2d.FontStyleItalic}
	buffer := RenderString("Gb", fontdata, 60, color.NRGBA{255, 255, 255, 0x80}, color.NRGBA{0x80, 0x80, 0xFF, 0x80}, colorMap[randInt(0, 6)])
	draw2dimg.SaveToPngFile("hello.png", buffer)
}

func TestGraphics(t *testing.T)  {
	goGraphics()
}

func TestDrawH(t *testing.T)  {
	draw2d.SetFontFolder(fontPath)
	var fontdata = draw2d.FontData{
		Name:   "luxi",
		Family: draw2d.FontFamilyMono,
		Style:  draw2d.FontStyleBold | draw2d.FontStyleItalic}
	buffer := drawH("Gb", fontdata, 65, color.NRGBA{255, 255, 255, 0x80}, color.NRGBA{0x80, 0x80, 0xFF, 0x80}, colorMap[randInt(0, 6)])
	draw2dimg.SaveToPngFile("back.png", buffer)
}

func TestGeo(t *testing.T) {
	dest := image.NewRGBA(image.Rect(0, 0, 512, 512))
	gc := draw2dimg.NewGraphicContext(dest)
	Draw(gc, 297, 210)
	CurveRectangle(gc, 300, 300, 100, 100, color.NRGBA{0x80, 0, 0, 0x80}, color.NRGBA{0x80, 0x80, 0xFF, 0xFF})
	gc.Close()
	gc.FillStroke()

	// Save to file
	draw2dimg.SaveToPngFile("geo.png", dest)
}