package main

import (
	"github.com/llgcode/draw2d/draw2dimg"
	"image"
	"image/color"
	"github.com/llgcode/draw2d"
	"math/rand"
	"time"
	"os"
	"image/jpeg"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

// stand for color red, orange, yellow, green, cyan, blue, purple
var colorMap = map[uint8]color.RGBA{
	0: color.RGBA{randInt(220, 240), randInt(40, 70), randInt(40, 70), 0xff},
	1: color.RGBA{randInt(230, 255), randInt(95, 135), randInt(4, 50), 0xff},
	2: color.RGBA{randInt(250, 255), randInt(245, 255), randInt(0, 80), 0xff},
	3: color.RGBA{randInt(30, 100), randInt(250, 255), randInt(0, 100), 0xff},
	4: color.RGBA{randInt(0, 100), randInt(250, 255), randInt(200, 220), 0xff},
	5: color.RGBA{randInt(0, 80), randInt(70, 130), randInt(220, 255), 0xff},
	6: color.RGBA{randInt(160, 200), randInt(0, 100), randInt(200, 255), 0xff}}

func coverSaveToGridFs(text string, dbName string, db *mgo.Session) (error, bson.ObjectId) {
	id := bson.NewObjectId()
	g, err := db.DB(dbName).GridFS("fs").Create(id)  // TODO need to check
	if err != nil {
		return err, nil
	}
	defer g.Close()
	fontData := setFont(nil)
	buffer := RenderString(text, fontData, 65, color.NRGBA{255, 255, 255, 0x80}, color.NRGBA{0x80, 0x80, 0xFF, 0x80}, colorMap[randInt(0, 6)])
	err = jpeg.Encode(g, buffer, nil)
	if err != nil {
		return err, nil
	}
	return nil, id
}

func setFont(extPath string) draw2d.FontData {
	var fontPath string
	if extPath == nil {
		fontPath = os.Getenv("GOPATH") + "/src/github.com/llgcode/draw2d/resource/font" // temporary solution
	}else {
		fontPath = os.Getenv("GOPATH") + extPath
	}

	draw2d.SetFontFolder(fontPath)
	var fontData = draw2d.FontData{
		Name:   "luxi",
		Family: draw2d.FontFamilyMono,
		Style:  draw2d.FontStyleBold | draw2d.FontStyleItalic}
	return fontData
}

func RenderString(text string, fd draw2d.FontData, size float64, c, stroke, fill color.Color) (buffer image.Image) {
	// for 150x150 avatar pixel
	const (
		stretchFactor = 1.75
		height = 150.0
		widthMax = 150.0
	)

	var widthStretch float64

	switch len(text) {
	case 1:
		widthStretch = widthMax / 3.5
	case 2:
		widthStretch = widthMax / 7
	case 3:
		widthStretch = 1
	}

	buf := image.NewRGBA(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})

	gc := draw2dimg.NewGraphicContext(buf)

	x0, y0 := 0.0, 0.0
	x1 := x0 + widthMax
	y1 := y0 + height
	gc.MoveTo(x0, (y0 + y1) / 2)
	gc.CubicCurveTo(x0, y0, x0, y0, (x0 + x1) / 2, y0)
	gc.CubicCurveTo(x1, y0, x1, y0, x1, (y0 + y1) / 2)
	gc.CubicCurveTo(x1, y1, x1, y1, (x1 + x0) / 2, y1)
	gc.CubicCurveTo(x0, y1, x0, y1, x0, (y0 + y1) / 2)
	gc.SetStrokeColor(stroke)
	gc.SetFillColor(fill)
	gc.SetLineWidth(10.0)
	gc.FillStroke()

	gc.Translate(widthStretch, height / stretchFactor)
	gc.SetFontData(fd)
	gc.SetFontSize(size)
	gc.SetFillColor(c)
	gc.FillString(text)

	buffer = buf.SubImage(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})
	return buffer
}

func randInt(min, max int) uint8 {
	rand.Seed(time.Now().UTC().UnixNano())
	return uint8(min + rand.Intn(max - min))
}