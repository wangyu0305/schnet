package main

import (
	"github.com/llgcode/draw2d/draw2dimg"
	"image"
	"image/color"
	"github.com/llgcode/draw2d"
	"os"
	"time"
	"math/rand"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"image/jpeg"
)

func courseCoverSaveToGridFs(text string, dbName string, db *mgo.Session) (error, bson.ObjectId) {
	id := bson.NewObjectId()
	g, err := db.DB(dbName).GridFS("fs").Create(id)  // TODO need to check
	if err != nil {
		return err, nil
	}
	defer g.Close()
	fontData := setFont(nil)
	buffer := generate(text, fontData, 65, nil, color.NRGBA{255, 255, 255, 0x80}, color.NRGBA{0x80, 0x80, 0xFF, 0x80}, colorMap[randInt(0, 6)])
	err = jpeg.Encode(g, buffer, nil)
	if err != nil {
		return err, nil
	}
	return nil, id
}

func generate(text string, fd draw2d.FontData, size float64, background string, c, stroke, fill color.Color) (buffer image.Image) {
	const stretchFactor = 1.25
	var widthStretch float64
	height := 125.0
	widthMax := 450.0

	switch len(text) {
	case 1:
		widthStretch = widthMax / 2
	case 2:
		widthStretch = widthMax / 2.5
	case 3:
		widthStretch = widthMax / 3
	case 4:
		widthStretch = widthMax / 4
	case 5:
		widthStretch = widthMax / 6
	case 6:
		widthStretch = widthMax / 8
	case 7:
		widthStretch = widthMax / 11
	case 8:
		widthStretch = widthMax / 15
	case 9:
		widthStretch = 5
	case 10:
		widthStretch = 1
	default:
		widthStretch = 0
	}

	if background == nil{background = "background1.png"}
	fImg1, _ := os.Open(background)
	defer fImg1.Close()
	img1, _, _ := image.Decode(fImg1)
	var b = img1.Bounds()
	var randX = randInt32(b.Min.X, b.Max.X - int(widthMax))
	var randY = randInt32(b.Min.Y, b.Max.Y - int(height))

	buf := image.NewRGBA(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})

	for x0, x := 0, randX; x0 <= int(widthMax); x++ {
		for y0, y := 0, randY; y0 <= int(height); y++ {
			col := img1.At(x, y)
			buf.Set(x0, y0, col)
			y0++
		}
		x0++
	}

	gc := draw2dimg.NewGraphicContext(buf)

	gc.Translate(widthStretch, height / stretchFactor)
	gc.SetFontData(fd)
	gc.SetFontSize(size)
	gc.SetFillColor(c)
	gc.FillString(text)

	buffer = buf.SubImage(image.Rectangle{
		Min: image.Point{0, 0},
		Max: image.Point{int(widthMax), int(height)},
	})
	// Save to file
	return buffer
}

func randInt32(min, max int) int {
	rand.Seed(time.Now().UTC().UnixNano())
	return int(min + rand.Intn(max - min))
}