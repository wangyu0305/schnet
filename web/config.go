package main

import (
	"encoding/json"
	_ "github.com/lib/pq"
	"os"
	"fmt"
	"time"
)

var conf = config("./db.json")

type Configuration struct {
	memcache string
	Mongo    Mongo
	Redis    Redis
}

type Mongo struct {
	HOST          string
	DB            string
	Files         string
	PicturesFiles string
}

type Redis struct {
	REDIS_SERVER    string
	REDIS_PORT      int
	REDIS_VERIFY_DB int
	REDIS_CHAT_DB   int
}

func config(filename string) Configuration {
	file, err := os.Open(filename)
	if err != nil {
		logging(fmt.Sprintf("ERROR %s: %s config file open error: %s \n", file_line(), time.Now(), err))
	}
	decoder := json.NewDecoder(file)
	conf := Configuration{}
	err = decoder.Decode(&conf)
	if err != nil {
		logging(fmt.Sprintf("ERROR %s: %s fail to load config file: %s  config: %s\n", file_line(), time.Now(), err, conf))
	}
	return conf
}
