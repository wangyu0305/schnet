package main

import (
	"gopkg.in/redis.v3"
	"fmt"
)

const day = 3600 * 24

var vClient = redis.NewClient(&redis.Options{
	Addr:     fmt.Sprintf("%s:%s", conf.Redis.REDIS_SERVER, conf.Redis.REDIS_PORT),
	Password: "", // no password set
	DB:       conf.Redis.REDIS_VERIFY_DB, // use default DB
})

var cClient = redis.NewClient(&redis.Options{
	Addr:     fmt.Sprintf("%s:%s", conf.Redis.REDIS_SERVER, conf.Redis.REDIS_PORT),
	Password: "", // no password set
	DB:       conf.Redis.REDIS_CHAT_DB, // use default DB
})

func getVCache(cacheId string) string {
	if cacheId != nil {
		res, err := vClient.Get(cacheId).Result()
		if err == nil {
			return res
		}
	}
	return nil
}

func setVCache(cacheId string, content string, expire int) bool {
	err := vClient.Set(cacheId, content, expire).Err()
	if err == nil {
		return true
	}
	return false
}

func getCCache(cacheId string) string {
	if cacheId != nil {
		res, err := cClient.Get(cacheId).Result()
		if err == nil {
			return res
		}
	}
	return nil
}

func setCCache(cacheId string, content string, expire int) bool {
	err := cClient.Set(cacheId, content, expire).Err()
	if err == nil {
		return true
	}
	return false
}