package main

import (
	"gopkg.in/mgo.v2/bson"
	"./db"
	"net/http"
	"time"
	"fmt"
	"html/template"
	"strings"
	"net/smtp"
	"encoding/json"
)

func home(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "GET":
		p := &Page{Title: r.URL.Path, Body: []byte("test")}
		t, err := template.ParseFiles("home.html")
		if err != nil {
			return http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		return t.Execute(w, p)
	}
	http.Redirect(w, r, "/404.html", 404)
}

func background(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "GET":
		username, login := checkLogin(r)
		if login {
			insName := r.FormValue("institution")
			s := mgoInit()
			defer mgoRelease(s)
			col := s.DB(conf.Mongo.HOST).C("institution")
			result := db.Institution{}
			err := col.Find(bson.M{"name": insName}).Select(bson.M{"_id": 1}).One(&result)
			if err != nil {
				logging(fmt.Sprintf(`db find error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			col = s.DB(conf.Mongo.DB).C("user")
			colQuerier := bson.M{"username": username}
			change := bson.M{"$set": bson.M{"institution": result.ID}}
			err = col.Update(colQuerier, change)
			if err != nil {
				logging(fmt.Sprintf(`db update error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			return http.RedirectHandler("/course", 200)
		}
	}
	http.NotFound(w, r)
}

func course(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "GET":
		username, login := checkLogin(r)
		if login {
			s := mgoInit()
			defer mgoRelease(s)
			userCol := s.DB(conf.Mongo.HOST).C("user")
			courseCol := s.DB(conf.Mongo.HOST).C("course")
			ures := db.User{}
			cres := []db.Course{}
			err := userCol.Find(bson.M{"username": username}).Select(bson.M{"institution": 1}).One(&ures)
			if err != nil {
				logging(fmt.Sprintf(`db find error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			err = courseCol.Find(bson.M{"institution": ures.Institution, "alive": true}).Sort("-create_at").All(&cres)
			if err != nil {
				logging(fmt.Sprintf(`db find error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			for _, c := range cres {
				c.Cover
				// TODO need template to deal with course result
			}
			//  TODO return html pattern
		}
	case r.Method == "POST":
		username, login := checkLogin(r)
		if login {
			code := strings.TrimSpace(r.FormValue("code"))  // exp busi1001
			name := strings.TrimSpace(r.FormValue("name"))
			insId := r.FormValue("institutionId")
			desc := r.FormValue("description")
			instructor := r.FormValue("instructor")
			section := strings.TrimSpace(r.FormValue("section"))
			schedule := r.FormValue("schedule")
			location := r.FormValue("location")
			s := mgoInit()
			defer mgoRelease(s)
			userCol := s.DB(conf.Mongo.HOST).C("user")
			courseCol := s.DB(conf.Mongo.HOST).C("course")
			insCol := s.DB(conf.Mongo.HOST).C("institution")
			chaCol := s.DB(conf.Mongo.HOST).C("chat")
			userRes := db.User{}
			err := userCol.Find(bson.M{"username": username}).One(&userRes)
			if err != nil {
				logging(fmt.Sprintf(`db find error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			CID := bson.NewObjectId()
			err, coverId := courseCoverSaveToGridFs(code, conf.Mongo.PicturesFiles, s)
			chaId := bson.NewObjectId()
			if err != nil {
				logging(fmt.Sprintf(`db upload to gridfs error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			courseRes := db.Course{ID: CID, Code: code, Name: name, Description: desc, Creator: username, Instructor: instructor, Section: section, Schedule: schedule, Location: location, Create_at: time.Now(), Recently_active_at: time.Now(), Users: []string{userRes.ID}, Institution: insId, Follower: []string{userRes.ID}, Alive: true, Cover: coverId, Chat: chaId}
			err = courseCol.Insert(courseRes)
			if err != nil {
				logging(fmt.Sprintf(`db insert error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			chaCol.Insert(&db.Chat{ID: chaId, CourseId: CID})
			colQuery := bson.M{"username": username}
			change := bson.M{"$addToSet": bson.M{"created_courses": CID}}
			err = userCol.Update(colQuery, change)
			if err != nil {
				logging(fmt.Sprintf(`db update error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			colQuery = bson.M{"_id": bson.ObjectIdHex(insCol)}
			change = bson.M{"$addToSet": bson.M{"courses": CID}}
			err = insCol.Update(colQuery, change)
			if err != nil {
				logging(fmt.Sprintf(`db update error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			//  TODO return html pattern
		}
	}
	http.NotFound(w, r)
}

// TODO third part account login will be implemented later
func account(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "POST":
		method := r.FormValue("type")
		if method == "create" {
			name := strings.TrimSpace(r.FormValue("name"))
			email := strings.TrimSpace(r.FormValue("email"))
			password := strings.TrimSpace(r.FormValue("password"))
			auth_token := strings.TrimSpace(r.FormValue("auth_token"))
			auth_source := strings.TrimSpace(r.FormValue("auth_source"))
			code := randInt32(1000, 9999)
			if (email == nil || password == nil || !verify(auth_token, auth_source, email, &password, code)) {
				logging(fmt.Sprintf(`create account error: user email or password nil`))
				return http.Error(w, "email or password not exist", http.StatusInternalServerError)
			}
			if name == nil {
				sEmail := strings.Split(email, "@")
				name = sEmail[0]
			}
			s := mgoInit()
			defer mgoRelease(s)
			err, coverId := coverSaveToGridFs(name, conf.Mongo.PicturesFiles, s)
			if err != nil {
				logging(fmt.Sprintf(`db upload to gridfs error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			userCol := s.DB(conf.Mongo.HOST).C("user")
			userRes := db.User{Username: name, Email: email, Password: encrypt(password), Identities: "unverified", Avatar: coverId, Auth_token:auth_token, Auth_source:auth_source, Date: time.Now(), Last_login: time.Now(), Create_at: time.Now()}
			err = userCol.Insert(userRes)
			if err != nil {
				logging(fmt.Sprintf(`db insert error (user account): %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}
			setVCache(email, string(code), day)
			// TODO a verify page including email
		}else if method == "modify" {
			username, login := checkLogin(r)
			if login{
				name := strings.TrimSpace(r.FormValue("name"))
				password := strings.TrimSpace(r.FormValue("password"))
				avatar := strings.TrimSpace(r.FormValue("avatar"))
				biography := strings.TrimSpace(r.FormValue("biography"))
				institution := strings.TrimSpace(r.FormValue("institution"))
				s := mgoInit()
				defer mgoRelease(s)
				userCol := s.DB(conf.Mongo.HOST).C("user")
				var mapSet map[string]string
				if name{mapSet["username"] = name}
				if password{mapSet["password"] = encrypt(password)}
				if avatar{mapSet["avatar"] = avatar} // TODO upload picture or upload stored picture ID ?
				if biography{mapSet["biography"] = biography}
				if institution{mapSet["institution"] = institution}
				colQuery := bson.M{"username": username}
				change := bson.M{"set": bson.M{mapSet}}
				err := userCol.Update(colQuery, change)
				if err != nil {
					logging(fmt.Sprintf(`db update error: %s`, err))
					return http.Error(w, err.Error(), http.StatusInternalServerError)
				}
				return http.RedirectHandler("/", 200)
			}
			http.NotFound(w, r)  // TODO should be login failure page

		}else if method == "verify"{
			code := strings.TrimSpace(r.FormValue("code"))
			email := strings.TrimSpace(r.FormValue("email"))
			storedCode := getVCache(email)
			if code == storedCode{
				s := mgoInit()
				defer mgoRelease(s)
				userCol := s.DB(conf.Mongo.HOST).C("user")
				colQuery := bson.M{"email": email}
				var change bson.M
				if strings.Contains(email, "carleton"){
					change = bson.M{"$set": bson.M{"identities": "normal", "institution": "carleton"}} // TODO should be various institution_id check
				}else{
					change = bson.M{"$set": bson.M{"identities": "normal"}}
				}
				err := userCol.Update(colQuery, change)
				if err != nil {
					logging(fmt.Sprintf(`db update error: %s`, err))
					return http.Error(w, err.Error(), http.StatusInternalServerError)
				}
				return http.RedirectHandler("/", 200)
			}
			http.NotFound(w, r)
		}
	}
}

func channel(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "GET":
		_, login := checkLogin(r)
		if login {
			courseId := r.FormValue("courseid")
			chats := getCCache(courseId)
			s := mgoInit()
			defer mgoRelease(s)
			dialog := db.DialogCollection.Pool
			if chats == nil{
				chaCol := s.DB(conf.Mongo.HOST).C("chat")
				chaRes := db.Chat{}
				err := chaCol.Find(bson.M{"courseId": courseId}).Select(bson.M{"dialogue": 1}).One(&chaRes)
				if err != nil {
					logging(fmt.Sprintf(`db dialogue find error: %s`, err))
					return http.Error(w, err.Error(), http.StatusInternalServerError)
				}
				chats = chaRes.Dialogue
				json.Unmarshal(dialog, chats)
			}
			couCol := s.DB(conf.Mongo.HOST).C("course")
			couRes := db.Course{}
			err := couCol.Find(bson.M{"_id": courseId}).One(&couRes)
			if err != nil {
				logging(fmt.Sprintf(`db dialogue find error: %s`, err))
				return http.Error(w, err.Error(), http.StatusInternalServerError)
			}

			// TODO resource list layout & chats
		}
	case r.Method == "POST":
		username, login := checkLogin(r)
		if login {
			username // TODO chat
		}
	}
}

func question(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "GET":
		username, login := checkLogin(r)
		if login {
			username // TODO chat
		}
	case r.Method == "POST":
		username, login := checkLogin(r)
		if login {
			username // TODO chat
		}
	}
}

func idea(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "GET":
		username, login := checkLogin(r)
		if login {
			username // TODO chat
		}
	case r.Method == "POST":
		username, login := checkLogin(r)
		if login {
			username // TODO chat
		}
	}
}

func login(w http.ResponseWriter, r *http.Request) {
	switch {
	case r.Method == "GET":
		p := &Page{Title: r.URL.Path, Body: []byte("test")}
		t, err := template.ParseFiles("login.html")
		if err != nil {
			return http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		t.Execute(w, p)
	case r.Method == "POST":
		name := r.FormValue("name")
		pass := r.FormValue("password")
		redirectTarget := "/"
		if name != "" && pass != "" {
			if func(n string, p string) bool {
				s := mgoInit()
				defer mgoRelease(s)
				col := s.DB(conf.Mongo.HOST).C("user")
				result := db.User{}
				err := col.Find(bson.M{"username": n, "password": encrypt(p)}).Select(bson.M{"identities": 1, "date": 1}).One(&result)
				if err != nil {
					logging(fmt.Sprintf(`db find error: %s`, err))
					return false
				}
				if result.Identities != "admin" || result.Identities != "normal" {
					logging(fmt.Sprintf(`db result error: %s`, err))
					return false
				}
				colQuerier := bson.M{"username": n, "password": encrypt(p)}
				change := bson.M{"$set": bson.M{"last_login": result.Date, "date": time.Now()}}
				err = col.Update(colQuerier, change)
				if err != nil {
					logging(fmt.Sprintf(`db update error: %s`, err))
					return false
				}
				return true
			}(name, pass) {
				pass = encrypt(pass)
				setSession(name, w)
				redirectTarget = "/course"
			}
		}
		http.Redirect(w, r, redirectTarget, 302)
	default:
		http.Redirect(w, r, "/404.html", 404)
	}
}

func logout(w http.ResponseWriter, r *http.Request) {
	switch {
	case r.Method == "POST":
		_, c, _ := getSession(r)
		clearSession(w, c)
	}
	http.Redirect(w, r, "/", 302)
}

func verify(token, source, email string, password *string, code int) bool {
	*password = encrypt(*password)
	body := fmt.Sprintf(`Dear user, glad to send this email to you, your verify code is %d`, code)
	if !send(body, email){return false}
	return true
}

func send(body, to string) bool {
	from := "smylicustomer@gmail.com"
	pass := "smyliadmin"

	msg := "From: " + from + "\n" +
	"To: " + to + "\n" +
	"Subject: account verify\n\n" +
	body

	err := smtp.SendMail("smtp.gmail.com:587",
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		logging(fmt.Sprintf(`email send error: %s`, err))
		return false
	}
	return true
}
