package db

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
	"crypto/md5"
	"encoding/hex"
)

/*
	DB structure
	user -> course, institution, resource
	course -> institution, question, idea, chat, resource
	chat -> user, course
*/

type User struct {
	ID              bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Username        string `json:"username" bson:"username"`
	Email           string `json:"email" bson:"email"`
	Password        string `json:"password" bson:"password"`
	Biography       string `json:"biography" bson:"biography"`
	Auth_token      string `json:"auth_token" bson:"auth_token"`
	Auth_source     string `json:"auth_source" bson:"auth_source"`
	Identities      string `json:"identities" bson:"identities"`
	Stars           int `json:"stars" bson:"stars"`
	Avatar          string `json:"avatar" bson:"avatar"`
	Current_courses []string `json:"current_courses" bson:"current_courses"` // also known as followed course
	Joined_courses  []string `json:"joined_courses" bson:"joined_courses"`
	Created_courses []string `json:"created_courses" bson:"created_courses"`
	Questions       []string `json:"questions" bson:"questions"`
	Answers         []map[string]string `json:"answers" bson:"answers"`
	Ideas           []string `json:"ideas" bson:"ideas"`
	Notifiers       []string `json:"notifiers" bson:"notifiers"`             // users who want to be noticed
	Institution     string `json:"institution" bson:"institution"`
	Date            time.Time `json:"date" bson:"date"`                      // current_login date
	Password_reset  time.Time `json:"password_reset" bson:"password_reset"`
	Last_login      time.Time `json:"last_login" bson:"last_login"`
	Create_at       time.Time `json:"create_at" bson:"create_at"`
}

type Course struct {
	ID                 bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name               string `json:"name" bson:"name"`
	Code               string `json:"code" bson:"code"` // exp busi1003
	Description        string
	Instructor         string
	Section            string
	Schedule           []time.Time
	Location           string
	Creator            string
	Create_at          time.Time
	End_at             time.Time
	Recently_active_at time.Time
	Users              []string                         // course schedule noticed user
	Institution        string
	Cover              string
	Chat	string
	Questions          []string
	Ideas              []string
	Follower           []string                         // for those who want to be noticed if there is new information
	Resources          []string
	Alive              bool
}

type Institution struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Name        string
	Courses     []string
	Alias       []string
	Relations   []string
	Description string
	Address     string
	Founded     string
}

type Question struct {
	ID             bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Title          string
	Description    string
	Creator        string
	Answer         map[string]string
	Helpers        []map[string]string
	Course         string
	Followers      []string
	Level          int
	Solved         bool
	Publish        bool
	Contact_helper string
	Create_at      time.Time
}

type Idea struct {
	ID          bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Title       string
	Description string
	Creator     string
	Comments    []map[string]string
	Course      string
	Followers   []string
	Level       int
	Publish     bool
	Create_at   time.Time
}

type Chat struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	CourseId  string
	Dialogue  []map[string]string   // {userId: {dialogue: "", date: ""}}
}

// Resource for course (public)
type Resource struct {
	ID        bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Rtype     string // resource type
	Stype     string // more specific
	Title     string
	Filename  string
	Creator   string
	Rid       string // resource id from DB
	Upload_at time.Time
	Course    string
}

// TODO resource for user space, storage (private, 100M... a file structure)

type Dialog struct {
	Dialogue string
	date time.Time
}

type DialogCollection struct {
	Pool map[string]Dialog
}

func newCollection(isDrop bool, dbName string, collectionName []string) {
	mongoSession, err := mgo.Dial(conf.Mongo.HOST)
	if err != nil {
		panic(err)
	}
	defer mongoSession.Close()
	mongoSession.SetSafe(&mgo.Safe{})

	if isDrop {
		err = mongoSession.DB(dbName).DropDatabase()
		if err != nil {
			panic(err)
		}
		indexes := fullIndex()
		for _, c := range collectionName {
			col := mongoSession.DB(dbName).C(c)
			err = col.EnsureIndex(indexes[c])
			if err != nil {
				panic(err)
			}
		}
	}
}

func fullIndex() map[string]mgo.Index {
	userIndex := mgo.Index{
		Key:        []string{"username", "email", "institution"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	courseIndex := mgo.Index{
		Key:        []string{"name", "users", "questions", "ideas", "resources"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	instIndex := mgo.Index{
		Key:        []string{"name", "course", "alias"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	qirIndex := mgo.Index{
		Key:        []string{"title", "course"}, // index for questions, ideas, resources
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
	indexes := map[string]mgo.Index{"user": userIndex, "course": courseIndex, "institution": instIndex, "questions": qirIndex, "ideas": qirIndex, "resources": qirIndex}
	return indexes
}

func indexMaker(name []string) mgo.Index {
	return mgo.Index{
		Key:        name,
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}