package db

import (
	"fmt"
	"os"
	"log"
	"bufio"
	"runtime"
)

func logging(msg string)  {
	var f *os.File
	var err error
	if _, err = os.Stat("db.log"); os.IsNotExist(err) {
		// web.log does not exist
		f, err = os.OpenFile("db.log", os.O_RDWR|os.O_APPEND|os.O_CREATE, 0666)
	}else{
		f, err = os.OpenFile("db.log", os.O_RDWR|os.O_APPEND, 0666)
	}
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	b := bufio.NewWriter(f)
	defer func() {
		if err = b.Flush(); err != nil {
			log.Fatal(err)
		}
	}()

	_, err = fmt.Fprintf(b, msg)
	if err != nil {
		log.Fatal(err)
	}
}

func file_line() string {
	_, fileName, fileLine, ok := runtime.Caller(1)
	var s string
	if ok {
		s = fmt.Sprintf("%s:%d", fileName, fileLine)
	} else {
		s = ""
	}
	return s
}