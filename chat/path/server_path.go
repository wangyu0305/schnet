package main

import (
	"bufio"
	"fmt"
	"net/http"
	"time"
	"io"
)

//!+broadcaster
type client chan<- string // an outgoing message channel

var (
	entering = make(chan client)
	leaving  = make(chan client)
	messages = make(chan string) // all incoming client messages
)

func broadcaster() {
	clients := make(map[client]bool) // all connected clients
	for {
		select {
		case msg := <-messages:
		// Broadcast incoming message to all
		// clients' outgoing message channels.
			for cli := range clients {
				cli <- msg
			}

		case cli := <-entering:
			clients[cli] = true

		case cli := <-leaving:
			delete(clients, cli)
			close(cli)
		}
	}
}

//!-broadcaster

//!+handleConn
func handleConn(w http.ResponseWriter, r *http.Request) {
	ch := make(chan string) // outgoing client messages
	go clientWriter(w, ch)

	who := r.RemoteAddr
	ch <- "You are " + who
	messages <- who + " has arrived"
	entering <- ch


	input := bufio.NewScanner(conn)
	for input.Scan() {
		messages <- who + ": " + input.Text()
	}
	// NOTE: ignoring potential errors from input.Err()

	leaving <- ch
	messages <- who + " has left"
}

func clientWriter(w http.ResponseWriter, ch <-chan string) {
	for msg := range ch {
		io.WriteString(w, msg) // NOTE: ignoring network errors
	}
}

//!-handleConn

//!+main
func main() {
	http.HandleFunc("/", handleConn)
	err := http.ListenAndServeTLS("8000", "server.crt", "server.key", nil)
	if err != nil {
		fmt.Sprintf("ERROR: certificate error : date=%s, msg=%s", time.Now(), err)
	}
//	cer, err := tls.LoadX509KeyPair("server.crt", "server.key")
//	if err != nil {
//		fmt.Println(err)
//		return
//	}
//
//	config := &tls.Config{Certificates: []tls.Certificate{cer}}
//	listener, err := tls.Listen("tcp", ":8000", config)
//	if err != nil {
//		log.Println(err)
//		return
//	}
//	defer listener.Close()
//
	go broadcaster()
//	for {
//		conn, err := listener.Accept()
//		if err != nil {
//			log.Print(err)
//			continue
//		}
//		go handleConn(conn)
//	}
}
