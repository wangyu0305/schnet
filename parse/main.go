package main
import (
	"fmt"
	"log"
	"strings"
	"github.com/PuerkitoBio/goquery"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
	"net/http"
	"image"
	"bytes"
	"image/jpeg"
	_ "image/png"
	"github.com/nfnt/resize"
	"crypto/md5"
	"encoding/hex"
	"io/ioutil"
	"regexp"
//	"os"
	"crypto/tls"
	"sync"
	"runtime"
)

type IOS struct {
	ID            bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Title         string `json:"title" bson:"title"`
	Developer     string `bson:"developer" json:"developer"`
	AppName       string `bson:"appName" json:"appName"`
	Free          bool `bson:"free" json:"free"`
	Price         string `bson:"price" json:"price"`
	Likes         int `bson:"likes" json:"likes"`
	Downloads     int `bson:"downloads" json:"downloads"`
	AppSize       string `bson:"appSize" json:"appSize"`
	ReservesId    interface{} `bson:"reservesId" json:"reservesId"`
	VideoList     []interface{} `bson:"videoList" json:"videoList"`
	YtbVideos     []interface{} `bson:"ytbVideos" json:"ytbVideos"`
	IconId        bson.ObjectId `bson:"iconId" json:"iconId"`
	PictureId     bson.ObjectId `bson:"pictureId" json:"pictureId"`
	Uptime        time.Time `bson:"uptime" json:"uptime"`
	AppType       string `bson:"appType" json:"appType"`
	Category       string `bson:"category" json:"category"`
	Description   string `bson:"description" json:"description"`
	Tag           string `bson:"tag" json:"tag"`
	Views         int `bson:"views" json:"views"`
	Coins         int `bson:"coins" json:"coins"`
	Version       string `bson:"version" json:"version"`
	OsVersion     string `bson:"osVersion" json:"osVersion"`
	OriginUrl string `bson:"originUrl" json:"originUrl"`
	Contributor   string `bson:"contributor" json:"contributor"`
}

type Pic struct {
	ID            bson.ObjectId `json:"id" bson:"_id,omitempty"`
	IconId        bson.ObjectId `bson:"iconId" json:"iconId"`
	IconPixes     map[string]bson.ObjectId `bson:"iconPixes" json:"iconPixes"`
	Pictures      []map[string]bson.ObjectId `bson:"pictures" json:"pictures"`
	PicturesCount int `bson:"picturesCount" json:"picturesCount"`
}

const (
	HOST = "localhost:27017"
	DB = "Decorations"
	table_app = "apps"
	table_pic = "pictures"
	picture_files = "Decoration_Icon_Files"
)

var wg sync.WaitGroup

func ExampleScrape(url string) {
	runtime.GOMAXPROCS(4)
	mongoSession, err := mgo.Dial(HOST)
	if err != nil {
		log.Fatal(`db connect error: `)
	}
	defer mongoSession.Close()
	col := mongoSession.DB(DB).C(table_app)
	pic := mongoSession.DB(DB).C(table_pic)
	unique := make(map[string]bool)
	doc, err := goquery.NewDocument(url)
	if err != nil {
		log.Fatal(err)
	}
	content := doc.Find("div.section-content")

	content.Find("a").Each(func(i int, s *goquery.Selection) {
		title, _ := s.Attr("href")
		if !unique[title] && strings.Index(title, "/app/") > 0 {
			unique[title] = true
		}
	})
	fmt.Println(len(unique))
	count := 0
	for i, _ := range (unique) {
		wg.Add(1)
		d, err := goquery.NewDocument(i)
		if err != nil {
			fmt.Printf("ducoment parse error %s\n", err)
		}
		go transfer(i, pic, col, mongoSession, d)
		count++
		if(count % 10 == 0){
			wg.Wait()
			fmt.Println(count)
		}
	}
	wg.Wait()
}

func transfer(url string, pic, col *mgo.Collection, db *mgo.Session, doc *goquery.Document) {
	var osVersion string
	var desc string
	var newIcon string
	var nsize, nversion string
	var free bool
	minLetterDesc := 100
	defer wg.Done()
	title := doc.Find("div.left").Find("h1").Text()
	developer := doc.Find("div.left").Find("h2").Text()
	doc.Find("span").Each(
		func(i int, s *goquery.Selection) {
			if t, _ := s.Attr("itemprop"); t == "operatingSystem" {
				osVersion = s.Text()
			}
		})
	doc.Find("p").Each(
		func(i int, s *goquery.Selection) {
			if t, _ := s.Attr("itemprop"); t == "description" {
				text := s.Text()
				if (len(text) > minLetterDesc) {
					desc = text[:strings.LastIndex(text[:minLetterDesc], " ")]
				}else {
					desc = text
				}
			}
		})
	r, _ := regexp.Compile("Version:(.*?)Languages")
	jcon, _ := regexp.Compile("icon(.*?).jpeg")
	pcon, _ := regexp.Compile("icon(.*?).png")
	size := doc.Find("ul.list").Text()
	newSize := r.FindString(size)
	arraySize := strings.Split(newSize, "Size:")
	icon, _ := doc.Find("div.artwork").Find("meta").Attr("content")
	price := doc.Find("div.price").Text()
	if(strings.Index(strings.ToLower(price), "free") > -1){
		free = true
	}else {
		free = false
	}

	if (strings.Index(icon, "jpeg") > 0) {
		newIcon = jcon.ReplaceAllString(icon, "icon512x512.jpeg")
	}else {
		newIcon = pcon.ReplaceAllString(icon, "icon512x512.png")
	}
	if (len(arraySize) == 2) {
		nsize = strings.Trim(strings.Replace(arraySize[1], "Languages", "", -1), " ")
		nsize = nsize[:strings.Index(nsize, "MB")+2]
		nversion = strings.Trim(strings.Replace(arraySize[0], "Version:", "", -1), " ")
	}else {
		nsize = ""
		nversion = ""
	}
	result := &IOS{}
	err := col.Find(bson.M{"appName": title}).Select(bson.M{"version": 1, "appSize": 1}).One(&result)
	if err != nil {
//		fmt.Printf(`db find error: %s\n`, err)
	}else if result.Version == nversion && result.AppSize == nsize {
		fmt.Printf(`db result exist:\n`)
		return
	}else {
		deleteResource(title, pic, col, db)
	}

	developer = strings.Trim(strings.Replace(developer, "By", "", -1), " ")

	pixes := make(map[string]bson.ObjectId)
	pixes["512x512"] = toGridfs(newIcon, db)
	pixes["340x340"] = resizeIcon(newIcon, db, 340, 340)
	pixes["360x225"] = resizeIcon(newIcon, db, 360, 225)
	pictures := digitPic(doc, db)
	pid := bson.NewObjectId()
	err = pic.Insert(&Pic{ID:pid,
		IconId: pixes["360x225"],
		IconPixes: pixes,
		Pictures: pictures,
		PicturesCount: len(pictures)})
	if err != nil {
		fmt.Printf("picture insert error: %s", err)
		return
	}
	err = col.Insert(&IOS{
		Title: title,
		Developer: developer,
		AppName: title,
		Free: free,
		Price: price,
		Likes: 0,
		Downloads: 0,
		AppSize: nsize,
		ReservesId: "",
		IconId: pixes["360x225"],
		PictureId: pid,
		Uptime: time.Now(),
		AppType: "",
		Description: desc,
		Tag: "",
		Views: 0,
		Coins: 0,
		Version: nversion,
		OsVersion: osVersion,
		OriginUrl: url,
		Contributor: "itunes" })
	if err != nil {
		fmt.Printf("app insert error :%s", err)
		return
	}
}

func deleteResource(name string, pic, col *mgo.Collection, db *mgo.Session){
	result := &IOS{}
	err := col.Find(bson.M{"appName": name}).One(&result)
	if err != nil {
		fmt.Println(err)
		return
	}
	picResource := &Pic{}
	err = pic.Find(bson.M{"_id": result.PictureId}).One(&picResource)
	if err != nil{
		fmt.Println(err)
		return
	}
	for _, x := range(picResource.Pictures){
		for _, value := range x{
			err = db.DB(picture_files).GridFS("fs").RemoveId(value)
			if err != nil{
				fmt.Printf("pictures resource remove error: %s", err)
			}
		}
	}
	for _, x := range picResource.IconPixes{
		err = db.DB(picture_files).GridFS("fs").RemoveId(x)
	}
	err = db.DB(picture_files).GridFS("fs").RemoveId(picResource.IconId)
	if err != nil{
		fmt.Printf("picture resource remove error: %s", err)
	}
	err = pic.RemoveId(picResource.ID)
	if err != nil{
		fmt.Printf("picture details remove error: %s", err)
	}
	err = col.RemoveId(result.ID)
	if err != nil{
		fmt.Printf("app details remove error: %s", err)
	}
}

func resizeIcon(url string, db *mgo.Session, width, height uint) bson.ObjectId {
	id := bson.NewObjectId()
	filename := GetMD5Hash(url)
	g, err := db.DB(picture_files).GridFS("fs").Create(filename)  // TODO need to check
	if err != nil {
		fmt.Printf("fs create error! %s", err)
		return ""
	}
	defer g.Close()
	g.SetId(id)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	res, err := client.Get(url)
	if res.StatusCode != 200 || err != nil {
		return ""
	}
	img, _, err := image.Decode(res.Body)
	if err != nil {
		fmt.Println(err)
		return ""
	}

	newImage := resize.Resize(width, height, img, resize.Lanczos3)
	err = jpeg.Encode(g, newImage, nil)
	return id
}

func digitPic(doc *goquery.Document, db *mgo.Session) []map[string]bson.ObjectId {
	picArray := make([]map[string]bson.ObjectId, 0)
	count := 0
	doc.Find("div.lockup").Each(func(i int, s *goquery.Selection) {
		t, _ := s.Find("img").Attr("itemprop")
		m, _ := s.Find("img").Attr("alt")
		if strings.Index(m, "iPhone") != -1 && t == "screenshot" {
			title, _ := s.Find("img").Attr("src")
			index := strings.LastIndex(title, "screen")
			pixes := title[index + len("screen"):len(title) - len(".jpeg")]
			pictures := map[string]bson.ObjectId{pixes: toGridfs(title, db)}
			picArray = append(picArray, pictures)
			count++
		}
	})
	return picArray
}

func toGridfs(picUrl string, db *mgo.Session) bson.ObjectId {
	filename := GetMD5Hash(picUrl)
	id := bson.NewObjectId()
	g, err := db.DB(picture_files).GridFS("fs").Create(filename)  // TODO need to check
	if err != nil {
		fmt.Printf("fs create error: %s", err)
		return ""
	}
	defer g.Close()
	g.SetId(id)
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}
	client := &http.Client{Transport: tr}
	res, err := client.Get(picUrl)
	if err != nil || res.StatusCode != 200 {
		fmt.Print("http get error: %s", err)
		return ""
	}
	data, _ := ioutil.ReadAll(res.Body)
	image, _, err := image.Decode(bytes.NewReader(data))
	err = jpeg.Encode(g, image, nil)
	return id
}

func GetMD5Hash(text string) string {
	hasher := md5.New()
	hasher.Write([]byte(text))
	return hex.EncodeToString(hasher.Sum(nil))
}

func main() {
	ExampleScrape("http://www.apple.com/ca/itunes/charts/free-apps/")
	ExampleScrape("http://www.apple.com/ca/itunes/charts/paid-apps/")
}

