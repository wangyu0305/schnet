package main
import (
	"io/ioutil"
	"net/http"
	"encoding/json"
	"bytes"
	"strings"
	"net/url"
	"errors"
	"os"
	"fmt"
	"strconv"
	"flag"
)

type Video struct {
	Title     string
	url       string
	quality   string
	extension string
}

type VideoList struct {
	Title   string
	Videos  []Video
}

func (video *Video) FindMissingFields() (missingFields []string) {
	if video.quality == "" {
		missingFields = append(missingFields, "quality")
	}
	if video.extension == "" {
		missingFields = append(missingFields, "video type")
	}
	if video.url == "" {
		missingFields = append(missingFields, "url")
	}
	return
}

func (video *Video) Download(rep string) error {
	//Get video from url
	body, err := GetHttpFromUrl(video.url)
	fmt.Println(video.url)
	return nil
	if err != nil {
		fmt.Println(err)
		return err
	}
	if rep != "" {
		//Make a directory and give every user highest permission
		os.MkdirAll(rep, 0777)
		if !strings.HasSuffix(rep, "/") {
			rep += "/"
		}
	}

	filename := video.Title[:strings.Index(video.Title, " ")] + video.extension
	//Make sure there is no invalid characters in filename
	filename = strings.Map(
		func(r rune) rune {
			switch r {
			case '/', '\\':
				r = '.'
			case ':', '*', '?', '"', '<', '>', '|':
				r = '-'
			}
			return r
		}, filename)
	filename = rep + filename
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	_, err = file.Write(body)
	if err != nil {
		return err
	}
	return nil
}

func (vl *VideoList) Append(v Video) {
	v.Title = vl.Title
	vl.Videos = append(vl.Videos, v)
}

func (vl *VideoList) Download(rep string, quality, extension string) (err error) {
	vl.Filter(quality, extension)

	//No matter how many left, pick the first one
	video := vl.Videos[0]
	err = video.Download(rep)
	return err
}

func (vl *VideoList) Filter(quality, extension string) (err error) {
	var matchingVideos []Video
	//Filter by quality
	if quality != "" {
		for _, video := range vl.Videos {
			if video.quality == quality {
				matchingVideos = append(matchingVideos, video)
			}
		}
		vl.Videos = matchingVideos
	}
	matchingVideos = nil
	//Filter by extension
	if extension != "" {
		for _, video := range vl.Videos {
			if video.extension == extension {
				matchingVideos = append(matchingVideos, video)
			}
		}
		vl.Videos = matchingVideos
	}
	if len(vl.Videos) == 0 {
		err = errors.New(fmt.Sprintf("{_quality: %s, _extension: %s}", quality, extension))
		return
	}
	return
}

func (vl VideoList) String() string {
	var videoListStr string
	videoListStr += fmt.Sprintf("video Title: " + vl.Title + "\n")
	videoListStr += fmt.Sprintf("Index\tquality\textension\n")
	for idx, video := range vl.Videos {
		videoListStr += fmt.Sprintf(" %v\t%v\t%v\n",
			strconv.Itoa(idx),
			video.quality,
			video.extension)
	}
	return videoListStr
}

func GetHttpFromUrl(url string) (body []byte, err error) {
	resp, err := http.Get(url)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	body, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	return
}

func GetVideoListFromId(id string) (VideoList, error) {
	url := "https://www.youtube.com/watch?v=" + id
	return GetVideoListFromUrl(url)
}

func GetVideoListFromUrl(url string) (vl VideoList, err error) {
	//Get webpage content from url
	body, err := GetHttpFromUrl(url)
	if err != nil {
		return
	}
	//Extract json data from webpage content
	jsonData, err := GetJsonFromHttp(body)
	if err != nil {
		return
	}
	//Fetch video list according to json data
	vl, err = GetVideoListFromJson(jsonData)
	if err != nil {
		return
	}
	return
}

func GetJsonFromHttp(httpData []byte) (map[string]interface{}, error) {
	//Find out if this page is age-restricted
	if bytes.Index(httpData, []byte("og:restrictions:age")) != -1 {
		return nil, errors.New("this page is age-restricted")
	}
	//Find begining of json data
	jsonBeg := "ytplayer.config = {"
	beg := bytes.Index(httpData, []byte(jsonBeg))
	if beg == -1 { //pattern not found
		return nil, errors.New(fmt.Sprintf("{_pattern: %s}", jsonBeg))
	}
	beg += len(jsonBeg) //len(jsonBeg) returns the number of bytes in jsonBeg

	//Find offset of json data
	unmatchedBrackets := 1
	offset := 0
	for unmatchedBrackets > 0 {
		nextRight := bytes.Index(httpData[beg+offset:], []byte("}"))
		if nextRight == -1 {
			return nil, errors.New("unmatched brackets")
		}
		unmatchedBrackets -= 1
		unmatchedBrackets += bytes.Count(httpData[beg+offset:beg+offset+nextRight], []byte("{"))
		offset += nextRight + 1
	}

	//Load json data
	var f interface{}
	err := json.Unmarshal(httpData[beg-1:beg+offset], &f)
	if err != nil {
		return nil, err
	}
	return f.(map[string]interface{}), nil
}


func GetVideoListFromJson(jsonData map[string]interface{}) (vl VideoList, err error) {
	args := jsonData["args"].(map[string]interface{})
	vl.Title = args["title"].(string)
	encodedStreamMap := args["url_encoded_fmt_stream_map"].(string)
	//Videos are seperated by ","
	videoListStr := strings.Split(encodedStreamMap, ",")
	for _, videoStr := range videoListStr {
		//Parameters of a video are seperated by "&"
		videoParams := strings.Split(videoStr, "&")
		var video Video
		for _, param := range videoParams {
			/*Unescape the url encoding characters.
			Only do it after seperation because
			there are "," and "&" escaped in url*/
			param, err = url.QueryUnescape(param)
			if err != nil {
				return
			}
			switch {
			case strings.HasPrefix(param, "quality"):
				video.quality = param[8:]
			case strings.HasPrefix(param, "type"):
				//type and codecs are seperated by ";"
				video.extension = strings.Split(param, ";")[0][5:]
			case strings.HasPrefix(param, "url"):
				video.url = param[4:]
			}
		}
		missingFields := video.FindMissingFields()
		if missingFields != nil {
			err = errors.New(fmt.Sprintf("{_fields: %s}", missingFields))
			return
		}
		vl.Append(video)
	}
	return
}

func main()  {
	var url string
	flag.StringVar(&url, "url", "", "video url")
	flag.Parse()

	if url == ""{
		url = "https://www.youtube.com/watch?v=nCkpzqqog4k"
	}
//	url := "https://www.youtube.com/watch?v=4IrK0c1zClc"
//	url = "https://www.youtube.com/watch?v=nCkpzqqog4k"
	vl, err := GetVideoListFromUrl(url)
	fmt.Printf("Downloading %v...\n", vl.Title)
	if(err != nil){
		fmt.Println("list video from url error")
	}
	err = vl.Download("", "medium", "")
	if(err != nil){
		fmt.Println("video download error")
	}
}