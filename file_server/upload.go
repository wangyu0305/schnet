package main

import (
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"time"
	"./db"
	"net/http"
	"io"
)

func upload(w http.ResponseWriter, r *http.Request, dbName string) bool {
	err := r.ParseMultipartForm(50000000)  //50M
	if err != nil {
		logging(fmt.Sprintf("ERROR: %s %s fail parse multi form ", time.Now(), err))
		return false
	}
	dbInstance := mgoInit()
	defer mgoRelease(dbInstance)
	m := r.MultipartForm
	files := m.File["myfiles"]
	for i, _ := range files {
		file, err := files[i].Open()
		defer file.Close()
		if err != nil {
			logging(fmt.Sprintf("ERROR: %s %s file upload", time.Now(), err))
			return false
		}
		rId := bson.NewObjectId()
		g, err := dbInstance.DB(dbName).GridFS("fs").Create(rId)  // TODO need to check
		defer g.Close()
		if err == nil {
			if _, err := io.Copy(g, file); err != nil {
				logging(fmt.Sprintf("ERROR: %s %s file failure storing to gridfs", time.Now(), err))
				return false
			}
			c := dbInstance.DB(dbName).C("resources")
			// TODO need more detail
			res := db.Resource{Title: r.URL.Path[len("/upload/files/"):], Rid: rId}
			if err = c.Insert(res); err != nil{
				logging(fmt.Sprintf("ERROR: %s %s insert file error", time.Now(), err))
				return false
			}
		}
	}
	return true
}