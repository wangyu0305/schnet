package main

import (
	"gopkg.in/redis.v3"
	"fmt"
	"gopkg.in/mgo.v2/bson"
	"time"
	"bytes"
)

const day = 3600 * 24

var client = redis.NewClient(&redis.Options{
	Addr:     fmt.Sprintf("%s:%s", conf.Redis.SERVER, conf.Redis.PORT),
	Password: "", // no password set
	DB:       conf.Redis.DB, // use default DB
})

func getCache(cacheId string) string {
	if cacheId != nil {
		res, err := client.Get(cacheId).Result()
		if err == nil {
			return res
		}
	}
	return nil
}

func setCache(cacheId string, content string, expire int) bool {
	err := client.Set(cacheId, content, expire).Err()
	if err == nil {
		return true
	}
	return false
}

func cache(cacheId, dbName string) string {
	content := getCache(cacheId)
	if content == nil {
		dbInstance := mgoInit()
		defer mgoRelease(dbInstance)
		g, err := dbInstance.DB(dbName).GridFS("fs").OpenId(bson.ObjectIdHex(cacheId))
		defer g.Close()
		if err == nil {
			content = make([]byte, g.Size())
			_, err = g.Read(content)
			if err == nil {
				content = string(content[:bytes.IndexByte(content, 0)])
				setCache(cacheId, content, day)
				return content
			}
			logging(fmt.Sprintf("ERROR: download image error : date=%s, msg=%s", time.Now(), err))
		}else {
			logging(fmt.Sprintf("ERROR: download image error : date=%s, msg=%s", time.Now(), err))
		}
		return nil
	}
	return content
}