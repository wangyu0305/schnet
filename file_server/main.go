package main

import (
	"gopkg.in/mgo.v2"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/gorilla/securecookie"
	"net/http"
	"time"
	"fmt"
	"regexp"
)

type route struct {
	pattern *regexp.Regexp
	handler http.Handler
}

type RegexpHandler struct {
	routes []*route
}

func (h *RegexpHandler) Handler(pattern *regexp.Regexp, handler http.Handler) {
	h.routes = append(h.routes, &route{pattern, handler})
}

func (h *RegexpHandler) HandleFunc(pattern *regexp.Regexp, handler func(http.ResponseWriter, *http.Request)) {
	h.routes = append(h.routes, &route{pattern, http.HandlerFunc(handler)})
}

func (h *RegexpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, route := range h.routes {
		if route.pattern.MatchString(r.URL.Path) {
			route.handler.ServeHTTP(w, r)
			return
		}
	}
	http.NotFound(w, r)
}

const (
	PORT = ":11443"
	PRIV_KEY = "./server.crt"
	PUBLIC_KEY = "./server.key"
)

var mc = memcache.New(conf.memcache)

var cookieHandler = securecookie.New(
	securecookie.GenerateRandomKey(64),
	securecookie.GenerateRandomKey(32))

func main() {
	reHandler := new(RegexpHandler)
	reHandler.HandleFunc("/files/(*)", downloadFile)
	reHandler.HandleFunc("/upload/files/(*)", uploadFile)
	err := http.ListenAndServeTLS(PORT, PRIV_KEY, PUBLIC_KEY, reHandler)
	if err != nil {
		logging(fmt.Sprintf("ERROR: certificate error : date=%s, msg=%s", time.Now(), err))
	}
}

func mgoInit() *mgo.Session {
	mongoSession, err := mgo.Dial(conf.Mongo["HOST"])
	if err != nil {
		panic(err)
	}
	mongoSession.SetSafe(&mgo.Safe{})
	return mongoSession
}

func mgoRelease(s *mgo.Session){
	s.Close()
}

func downloadFile(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "GET":
		if checkLogin(r) {
			content := cache(r.URL.Path, conf.Mongo.Files)
			if content == nil{
				return http.NotFound(w, r)
			}
			r.Header.Set("Content-Type", r.Header.Get("Content-Type"))
			r.Header.Set("Content-Disposition", fmt.Sprintf("attachment;filename=\"%s\"" , r.URL.Path[len("/files/"):]))
			return w.Write([]byte(content))
		}
	}
	http.NotFound(w, r)
}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	logging(fmt.Sprintf("%s %s %s", r.RemoteAddr, r.Method, r.URL))
	switch {
	case r.Method == "POST":
		if checkLogin(r) {
			if !upload(w, r, conf.Mongo.Files){
				return http.RedirectHandler("/", 500)
			}
		}
	}
	http.NotFound(w, r)
}

func checkLogin(r *http.Request) bool {
	_, c, err := getSession(r)
	if c != nil {
		login, err := mc.Get(c)
		if login.Object == true && err == nil {
			return true
		} else {
			logging(fmt.Sprintf("INFO: failure login attemp: date=%s msg=%s", time.Now(), err))
			return false
		}
	} else {
		logging(fmt.Sprintf("INFO: session error: date=%s msg=%s", time.Now(), err))
		return false
	}
}

func getSession(request *http.Request) (string, string, error) {
	if cookie, err := request.Cookie("session"); err == nil {
		cookieValue := make(map[string]string)
		if err = cookieHandler.Decode("session", cookie.Value, &cookieValue); err == nil {
			userName := cookieValue["name"]
			return userName, cookie, nil
		}
	}
	return
}
