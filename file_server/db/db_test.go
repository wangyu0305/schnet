package db

import (
	"testing"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"time"
	"fmt"
)

func TestIndex(t *testing.T) {
	newCollection(false, conf.Mongo.DB, []string{"user", "course", "institution", "questions", "ideas", "resources"})
}

func TestUser(t *testing.T) {
	mongoSession, err := mgo.Dial(conf.Mongo.HOST)
	if err != nil {
		t.Error(`db connect error: `, err)
	}
	defer mongoSession.Close()
	col := mongoSession.DB(conf.Mongo.TestDB).C("user")
	doc := User{Username: "Alex", Password: GetMD5Hash("abc"), Biography: "ryan", Answers: nil, Create_at: time.Now(), Date: time.Now(), Password_reset: time.Now(), Last_login: time.Now()}
	err = col.Insert(doc, &User{Username: "Bill", Password: GetMD5Hash("asd"), Biography: "wil", Answers: nil, Create_at: time.Now(), Date: time.Now(), Password_reset: time.Now(), Last_login: time.Now()})

	if err != nil {
		t.Error(`insert error: `, err)
	}

	result := User{}
	err = col.Find(bson.M{"username": "Alex"}).Select(bson.M{"biography": 1}).One(&result)
	if err != nil {
		t.Error(`db find error: `, err)
	}
	if result.Biography != "ryan"{
		t.Error(`db result error: `, err, " result：", result)
	}

	colQuerier := bson.M{"username": "Alex"}
	change := bson.M{"$set": bson.M{"biography": "ryanW", "date": time.Now()}}
	err = col.Update(colQuerier, change)
	if err != nil {
		t.Error(`db update error: `, err)
	}

	var results []User
	err = col.Find(bson.M{}).Sort("-create_at").All(&results)

	if err != nil {
		t.Error(`db findall error: `, err)
	}
	for _, r := range results {
		fmt.Println("username: ", r.Username, "password: ", r.Password)
	}
}

func TestIdeas(t *testing.T) {
	mongoSession, err := mgo.Dial(conf.Mongo.HOST)
	if err != nil {
		t.Error(`db connect error: `, err)
	}
	defer mongoSession.Close()
	col := mongoSession.DB(conf.Mongo.TestDB).C("ideas")
	doc := Idea{Title: "start", Description: "this is start idea", Creator: "ryan", Comments: nil, Course: "comp1234", Level: 1, Publish: false, Create_at: time.Now()}
	err = col.Insert(doc)

	if err != nil {
		t.Error(`insert error: `, err)
	}

	result := Idea{}
	err = col.Find(bson.M{"title": "start"}).Select(bson.M{"creator": 1}).One(&result)
	if err != nil {
		t.Error(`db find error: `, err)
	}
	if result.Creator != "ryan"{
		t.Error(`db result error: `, err, " result：", result)
	}

	colQuerier := bson.M{"title": "start"}
	change := bson.M{"$set": bson.M{"creator": "ryanW", "create_at": time.Now()}}
	err = col.Update(colQuerier, change)
	if err != nil {
		t.Error(`db update error: `, err)
	}

	var results []Idea
	err = col.Find(bson.M{}).Sort("-create_at").All(&results)

	if err != nil {
		t.Error(`db findall error: `, err)
	}
	for _, r := range results {
		fmt.Println("title: ", r.Title, "creator: ", r.Creator)
	}
}
