package db

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"time"
	"strings"
)

var DB_USER = conf.Postgresql.USER
var DB_PASSWORD = conf.Postgresql.PASSWORD
var DB_NAME = conf.Postgresql.DBNAME

var dbInstance *sql.DB

func pgInit() {
	var err error
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s sslmode=disable",
		DB_USER, DB_PASSWORD, DB_NAME)
	dbInstance, err = sql.Open("postgres", dbinfo)
	if err != nil {
		logging(fmt.Sprintf("ERROR: %s date=%s DB Init error msg=%s\n",file_line(), time.Now(), err))
		return
	}
	logging(fmt.Sprintf("-- initialize postgresql DB -- %s\n", time.Now()))
}

func pgClose() {
	dbInstance.Close()
}

func pgVerifyUser(username string, password string) bool {
	var verify string
	if strings.Index(username, "@") > 0 {
		verify = "email"
	}else {verify = "username"}
	rows, err := dbInstance.Query("SELECT (identities) from users where %s='%s' and hash_password='%s';", verify, username, password)
	if err != nil {
		return false
	}
	for rows.Next() {
		var identities string
		err = rows.Scan(&identities)
		if err == nil {
			return true
		}
	}
	return false
}
